//var marked = require('marked');
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird'); // changing from mpromise
mongoose.connect('mongodb://localhost/tripplanner');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'mongodb connection error:'));

/*--------- Page Schema ----------*/

var placeSchema = new mongoose.Schema({
  address:    {type: String},
  city:       {type: String},
  state:      {type: String},
  phone:      {type: String},
  location:   [Number] //latitude longitude array
  //_id: {type: ObjectId}
});

var hotelSchema = new mongoose.Schema({
  name:           {type: String},
  place:          [placeSchema],
  num_stars:      {type: Number, min: 0, max: 5},
  amenities:      {type: String} //comma delimited string list
});

var activitySchema = new mongoose.Schema({
  name:       {type: String},
  place:      [placeSchema],
  age_range:  {type: String} //data-type string
});

var restaurantSchema = new mongoose.Schema({
  name:       {type: String},
  place:      [placeSchema],
  cuisines:   {type: String}, //comma delimited string list
  price:      {type: Number, min: 1, max: 5}
});

/*----------- Models --------------*/

var Place = mongoose.model('Place', placeSchema);
var Hotel = mongoose.model('Hotel', hotelSchema);
var Activity = mongoose.model('Activity', activitySchema);
var Restaurant = mongoose.model('Restaurant', restaurantSchema);


module.exports = {
  Place: Place,
  Hotel: Hotel,
  Activity: Activity,
  Restaurant: Restaurant
};
