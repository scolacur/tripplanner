var express = require('express');
var router = express.Router();
var models = require('../models');
var Place = models.Place;
var Hotel = models.Hotel;
var Activity = models.Activity;
var Restaurant = models.Restaurant;
var nodemon = require("nodemon");

/* GET home page. */
router.get('/', function(req, res, next) {
  var _hotels;
  var _restaurants;
  var _activities;

  Hotel.find({}).exec()
  .then(function(hotels) {
    _hotels = hotels;
    return Restaurant.find({}).exec();
  })
  .then(function(restaurants) {
    _restaurants = restaurants;
    return Activity.find({}).exec();
  })
  .then(function(activities) {
    _activities = activities;
  //  console.log("HI");
    res.render('index', {
      title: 'Welcome to Trip Planner',
      all_hotels: _hotels,
      all_restaurants: _restaurants,
      all_activities: _activities
    });
  }).then(null, console.log);
});

module.exports = router;




  // Hotel.find({}).exec().then(function(hotels) {
  //     Restaurant.find({}).exec().then(function(restaurants) {
  //         Activity.find({}).exec().then(function(activities) {
  //           //console.log(hotels);
  //             res.render('index', {
  //                 title: 'Welcome to Trip Planner',
  //                 all_hotels: hotels,
  //                 all_restaurants: restaurants,
  //                 all_activities: activities
  //             });
  //         }).then(null, console.log);
  //     }).then(null, console.log);
  // }).then(null, console.log);
